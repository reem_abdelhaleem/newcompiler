import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavberComponent } from './layout/navber/navber.component';
import { FooterComponent } from './layout/footer/footer.component';
import { AboutComponent } from './about/about.component';
import { SliderComponent } from './home/components/slider/slider.component';
import { AboutSectionComponent } from './home/components/about-section/about-section.component';
import { ServicesComponent } from './home/components/services/services.component';
import { SubscribeComponent } from './home/components/subscribe/subscribe.component';
import { ContactUsSectionComponent } from './home/components/contact-us-section/contact-us-section.component';
import { SponsorsComponent } from './home/components/sponsors/sponsors.component';
import { TestmonialsComponent } from './home/components/testmonials/testmonials.component';
import { PostsComponent } from './home/components/posts/posts.component';
import { CounterComponent } from './home/components/counter/counter.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavberComponent,
    FooterComponent,
    AboutComponent,
    SliderComponent,
    AboutSectionComponent,
    ServicesComponent,
    SubscribeComponent,
    ContactUsSectionComponent,
    SponsorsComponent,
    TestmonialsComponent,
    PostsComponent,
    CounterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
